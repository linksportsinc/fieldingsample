import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  PanResponder,
  Image,
  Easing,
  Animated,
  TouchableOpacity
} from 'react-native';

const {width, height} = Dimensions.get('window');

const HOME_BASE = {x: 180, y:560};
const PITCHER_POSITION = {x:180, y:340};
const CATCHER_POSITION = {x:180, y:510};
const FIRST_BASE_POSITION = {x:300, y:370};
const SECOND_BASE_POSITION = {x:250, y:280};
const SHORT_STOP_POSITION = {x:110, y:280};
const THIRD_BASE_POSITION = {x:55, y:370};
const LEFT_FIELDER_POSITION = {x:40, y:220};
const CENTER_FIELDER_POSITION = {x:180, y:180};
const RIGHT_FIELDER_POSITION = {x:310, y:220};

class Fielding extends Component {

  constructor(props: any) {
    super(props);
    this.state = {
      gestureState: {},
      pitcherPos: new Animated.ValueXY(PITCHER_POSITION),
      catcherPos: new Animated.ValueXY(CATCHER_POSITION),
      firstBasePos: new Animated.ValueXY(FIRST_BASE_POSITION),
      secondBasePos: new Animated.ValueXY(SECOND_BASE_POSITION),
      shortStopPos: new Animated.ValueXY(SHORT_STOP_POSITION),
      thirdBasePos: new Animated.ValueXY(THIRD_BASE_POSITION),
      leftFielderPos: new Animated.ValueXY(LEFT_FIELDER_POSITION),
      centerFielderPos: new Animated.ValueXY(CENTER_FIELDER_POSITION),
      rightFielderPos: new Animated.ValueXY(RIGHT_FIELDER_POSITION),
      pan: new Animated.ValueXY(HOME_BASE),
      doneBtnVisible: false,
      thorowingBallVisible: false,
    };
  }

  componentWillMount() {
    // Responding to the current animation value
    // アニメーションを実行中の座標値を取得する方法の１つがaddListenerだが、守備では不要
    // this.state.pan.x.addListener((value) => this.setState({x: value.value}));
    // this.state.pan.y.addListener((value) => this.setState({y: value.value}));

    let {
      pitcherPos,
      catcherPos,
      firstBasePos,
      secondBasePos,
      thirdBasePos,
      shortStopPos,
      leftFielderPos,
      centerFielderPos,
      rightFielderPos,
      doneBtnVisible
    } = this.state;

    this.gestureResponderP = PanResponder.create({

      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('P', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });

    this.gestureResponderC = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('C', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });

    this.gestureResponder1B = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('1B', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });

    this.gestureResponder2B = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('2B', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });

    this.gestureResponderSS = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('SS', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });

    this.gestureResponder3B = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('3B', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });

    this.gestureResponderLF = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('LF', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });

    this.gestureResponderCF = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('CF', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });

    this.gestureResponderRF = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: (evt, gestureState) => {
      },
      onPanResponderMove: (evt, gestureState) => {
        this.move('RF', gestureState);
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        this.release(gestureState);
      },
      onPanResponderTerminate: (evt, gestureState) => {
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        return true;
      },
    });
  }

  componentWillUnmount() {
    // this.state.pan.x.removeAllListeners();
    // this.state.pan.y.removeAllListeners();
  }

  startFielding() {
    let { thorowingBallVisible } = this.state;
    thorowingBallVisible = true;
    this.setState({
      thorowingBallVisible
    });
  }

  move(pos, gestureState) {
    let {
        pitcherPos,
        catcherPos,
        firstBasePos,
        secondBasePos,
        shortStopPos,
        thirdBasePos,
        leftFielderPos,
        centerFielderPos,
        rightFielderPos,
        doneBtnVisible
    } = this.state;

    switch (pos) {
      case 'P':
        pitcherPos = {x:gestureState.moveX, y:gestureState.moveY};
        this.setState({pitcherPos});
        break;
      case 'C':
        catcherPos = {x:gestureState.moveX, y:gestureState.moveY};
        this.setState({catcherPos});
        break;
      case '1B':
        firstBasePos = {x:gestureState.moveX, y:gestureState.moveY};
        this.setState({firstBasePos});
        break;
      case '2B':
        secondBasePos = {x:gestureState.moveX, y:gestureState.moveY};
        this.setState({secondBasePos});
        break;
      case 'SS':
        shortStopPos = {x:gestureState.moveX, y:gestureState.moveY};
        this.setState({shortStopPos});
        break;
      case '3B':
        thirdBasePos = {x:gestureState.moveX, y:gestureState.moveY};
        this.setState({thirdBasePos});
        break;
      case 'LF':
        leftFielderPos = {x:gestureState.moveX, y:gestureState.moveY};
        this.setState({leftFielderPos});
        break;
      case 'CF':
         centerFielderPos = {x:gestureState.moveX, y:gestureState.moveY};
         this.setState({centerFielderPos});
         break;
      case 'RF':
        rightFielderPos = {x:gestureState.moveX, y:gestureState.moveY};
        this.setState({rightFielderPos});
        break;
    }

    // doneボタンを表示
    doneBtnVisible = true;
    this.setState({doneBtnVisible});
  }

  release(gestureState) {

    // 送球のボールを表示
    this.startFielding();

    // ドラッグを離した時点での画面上での絶対値で座標を取得し、
    // アニメーションの移動先として値を設定
    Animated.timing(this.state.pan, {
      toValue: {
        x: gestureState.moveX,
        y: gestureState.moveY,
      }
    }).start();
  }

  fieldingDone() {

    // doneボタンを非表示
    let {doneBtnVisible} = this.state;
    doneBtnVisible = false;
    this.setState({
      doneBtnVisible
    });

    //ボールをHOMEヘ移動&非表示
    let {thorowingBallVisible, pan} = this.state;
    pan = new Animated.ValueXY(HOME_BASE);
    thorowingBallVisible = false;
    this.setState({
      pan,
      thorowingBallVisible
    });

    //各守備位置を初期座標へ
    pitcherPos = {x:PITCHER_POSITION.x, y:PITCHER_POSITION.y};
    catcherPos = {x:CATCHER_POSITION.x ,y:CATCHER_POSITION.y};
    firstBasePos = {x:FIRST_BASE_POSITION.x ,y:FIRST_BASE_POSITION.y};
    secondBasePos = {x:SECOND_BASE_POSITION.x ,y:SECOND_BASE_POSITION.y};
    shortStopPos = {x:SHORT_STOP_POSITION.x ,y:SHORT_STOP_POSITION.y};
    thirdBasePos = {x:THIRD_BASE_POSITION.x ,y:THIRD_BASE_POSITION.y};
    leftFielderPos = {x:LEFT_FIELDER_POSITION.x ,y:LEFT_FIELDER_POSITION.y};
     centerFielderPos = {x:CENTER_FIELDER_POSITION.x ,y:CENTER_FIELDER_POSITION.y};
    rightFielderPos = {x:RIGHT_FIELDER_POSITION.x ,y:RIGHT_FIELDER_POSITION.y};

    this.setState({
      pitcherPos,
      catcherPos,
      firstBasePos,
      secondBasePos,
      shortStopPos,
      thirdBasePos,
      leftFielderPos,
      centerFielderPos,
      rightFielderPos
    });

  }

  render() {
    return (
      <View
        style={{flex: 1, backgroundColor: '#FFFFFF', padding: 20, alignItems: 'center', justifyContent: 'center'}}>

        <Image source={require('./Image/ground.png')} style={{width: 380, height: 580, marginTop: 550,}}>
          <View style={{marginTop:50}}>
            <LabelView
              label='moveX'
              value={this.state.gestureState.moveX}/>
            <LabelView
              label='moveY'
              value={this.state.gestureState.moveY}/>
            <LabelView
              label='dx'
              value={this.state.gestureState.dx}/>
            <LabelView
              label='dy'
              value={this.state.gestureState.dy}/>
          </View>
        </Image>

        {/* P */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state.pitcherPos.x,
          top: this.state.pitcherPos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponderP.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {/* C */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state.catcherPos.x,
          top: this.state.catcherPos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponderC.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {/* 1B */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state.firstBasePos.x,
          top: this.state.firstBasePos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponder1B.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {/* 2B */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state.secondBasePos.x,
          top: this.state.secondBasePos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponder2B.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {/* SS */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state.shortStopPos.x,
          top: this.state.shortStopPos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponderSS.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {/* 3B */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state.thirdBasePos.x,
          top: this.state.thirdBasePos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponder3B.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {/* LF */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state.leftFielderPos.x,
          top: this.state.leftFielderPos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponderLF.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {/* CF */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state. centerFielderPos.x,
          top: this.state. centerFielderPos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponderCF.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {/* RF */}
        <Animated.View
          style={{
          width: 20,
          height: 20,
          position: 'absolute',
          left: this.state.rightFielderPos.x,
          top: this.state.rightFielderPos.y,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000'
          }}
          {...this.gestureResponderRF.panHandlers}>
          <Image source={require('./Image/grove.png')} />
        </Animated.View>

        {this.state.thorowingBallVisible &&
          <Animated.View
            style={{
            position: 'absolute',
            left: this.state.pan.x,
            top: this.state.pan.y,
            }}>
           <Image
            source={require('./Image/ball.png')}
            style={{ width: 20, height: 20 }}/>
          </Animated.View>
        }

        {/* doneBtn*/}
        {this.state.doneBtnVisible &&
          <TouchableOpacity onPress={() => this.fieldingDone()}>
            <Image source={require('./Image/done.png')}
              style={{width: 40, height: 40, position: 'absolute', left:-20, top:-180}}
              />
          </TouchableOpacity>
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  //　スタイル
});

class LabelView extends Component {
  render() {
    return (
      <View
        style={{flexDirection: 'row', alignSelf: 'stretch', paddingLeft:30, paddingRight:30}}>
        <Text style={{flex: 4, textAlign: 'right', paddingRight: 10}}>{this.props.label}</Text>
        <Text style={{flex: 5, textAlign: 'left', paddingLeft: 10}}>{JSON.stringify(this.props.value)}</Text>
      </View>
    );
  }
}

export default class App extends React.Component {
  render() {
    return (
      <View>
        <Fielding />
      </View>
    )
  }
}
